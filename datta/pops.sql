BEGIN;
CREATE TABLE "nyc_pops" (gid serial PRIMARY KEY,
"Borough" varchar,
"CD_Number" int4,
"CD_Name" varchar,
"1970_Population" int8,
"1980_population" int8,
"1990_population" int8,
"2000_population" int8,
"2010_population" int8,

INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 1, 'Melrose; Mott Haven; Port Morris', 138557, 78441, 77214, 82159, 91497);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 2, 'Hunts Point; Longwood', 99493, 34399, 39443, 46824, 52246);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 3, 'Morrisania; Crotona Park East', 150636, 53635, 57162, 68574, 79762);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 4, 'Highbridge; Concourse Village', 144207, 114312, 119962, 139563, 146441);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 5, 'University Hts.; Fordham; Mt. Hope', 121807, 107995, 118435, 128313, 128200);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 6, 'East Tremont; Belmont', 114137, 65016, 68061, 75688, 83268);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 7, 'Bedford Park; Norwood; Fordham', 113764, 116827, 128588, 141411, 139286);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 8, 'Riverdale; Kingsbridge; Marble Hill', 103543, 98275, 97030, 101332, 101731);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 9, 'Soundview; Parkchester', 166442, 167627, 155970, 167859, 172298);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 10, 'Throgs Nk.; Co-op City; Pelham Bay', 84948, 106516, 108093, 115948, 120392);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 11, 'Pelham Pkwy; Morris Park; Laconia', 105980, 99080, 97842, 110706, 113232);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Bronx', 12, 'Wakefield; Williamsbridge', 135010, 128226, 129620, 149077, 152344);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 1, 'Williamsburg; Greenpoint', 179390, 142942, 155972, 160338, 173083);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 2, 'Brooklyn Heights; Fort Greene', 110221, 92732, 94534, 98620, 99617);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 3, 'Bedford Stuyvesant', 203380, 133379, 138696, 143867, 152985);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 4, 'Bushwick', 137902, 92497, 102572, 104358, 112634);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 5, 'East New York; Starrett City', 170791, 154931, 161350, 173198, 182896);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 6, 'Park Slope; Carroll Gardens', 138933, 110228, 102724, 104054, 104709);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 7, 'Sunset Park; Windsor Terrace', 111607, 98567, 102553, 120063, 126230);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 8, 'Crown Heights North', 121821, 88796, 96400, 96076, 96317);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 9, 'Crown Heights South; Wingate', 101047, 96669, 110715, 104014, 98429);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 10, 'Bay Ridge; Dyker Heights', 129822, 118187, 110612, 122542, 124491);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 11, 'Bensonhurst; Bath Beach ', 170119, 155072, 149994, 172129, 181981);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 12, 'Borough Park; Ocean Parkway', 166301, 155899, 160018, 185046, 191382);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 13, 'Coney Island; Brighton Beach', 97750, 100030, 102596, 106120, 104278);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 14, 'Flatbush; Midwood', 137041, 143859, 159825, 168806, 160664);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 15, 'Sheepshead Bay; Gerritsen Beach', 164815, 149572, 143477, 160319, 159650);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 16, 'Brownsville; Ocean Hill', 122589, 73801, 84923, 85343, 86468);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 17, 'East Flatbush; Rugby; Farragut', 149496, 154596, 161261, 165753, 155252);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Brooklyn', 18, 'Canarsie; Flatlands', 188643, 169092, 162428, 194653, 193543);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 1, 'Battery Park City; Tribeca', 7706, 15918, 25366, 34420, 60978);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 2, 'Greenwich Village; Soho', 84337, 87069, 94105, 93119, 90016);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 3, 'Lower East Side; Chinatown', 181845, 154848, 161617, 164407, 163277);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 4, 'Chelsea; Clinton', 83601, 82164, 84431, 87479, 103245);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 5, 'Midtown Business District', 31076, 39544, 43507, 44028, 51673);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 6, 'Stuyvesant Town; Turtle Bay', 122465, 127554, 133748, 136152, 142745);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 7, 'West Side; Upper West Side', 212422, 206669, 210993, 207699, 209084);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 8, 'Upper East Side', 200851, 204305, 210880, 217063, 219920);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 9, 'Manhattanville; Hamilton Heights', 113606, 103038, 106978, 111724, 110193);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 10, 'Central Harlem', 159267, 105641, 99519, 107109, 115723);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 11, 'East Harlem', 154662, 114569, 110508, 117743, 120511);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Manhattan', 12, 'Washington Heights; Inwood', 180561, 179941, 198192, 208414, 190020);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 1, 'Astoria; Long Island City', 185925, 185198, 188549, 211220, 191105);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 2, 'Sunnyside; Woodside', 95073, 88927, 94845, 109920, 113200);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 3, 'Jackson Heights; North Corona', 123635, 122090, 128924, 169083, 171576);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 4, 'Elmhurst; South Corona', 108233, 118430, 137023, 167005, 172598);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 5, 'Ridgewood; Glendale; Maspeth', 161022, 150142, 149126, 165911, 169190);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 6, 'Forest Hills; Rego Park', 120429, 112245, 106996, 115967, 113257);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 7, 'Flushing; Bay Terrace', 207589, 204785, 220508, 242952, 247354);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 8, 'Fresh Meadows; Briarwood', 142468, 125312, 132101, 146594, 151107);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 9, 'Woodhaven; Richmond Hill', 110367, 109505, 112151, 141608, 143317);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 10, 'Ozone Park; Howard Beach', 113857, 105651, 107768, 127274, 122396);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 11, 'Bayside; Douglaston; Little Neck', 127883, 110963, 108056, 116404, 116431);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 12, 'Jamaica; St. Albans; Hollis', 206639, 189383, 201293, 223602, 225919);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 13, 'Queens Village; Rosedale', 184647, 173178, 177535, 196284, 188593);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Queens', 14, 'The Rockaways; Broad Channel', 98228, 100592, 100596, 106686, 114978);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Staten Island', 1, 'Stapleton; Port Richmond', 135875, 138489, 137806, 162609, 175756);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Staten Island', 2, 'New Springville; South Beach', 85985, 105128, 113944, 127071, 132003);
INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('Staten Island', 3, 'Tottenville; Woodrow; Great Kills', 72815, 108249, 126956, 152908, 160209);
COMMIT;
