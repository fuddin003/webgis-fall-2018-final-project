echo "BEGIN;";
echo 'CREATE TABLE "nyc_pops" (gid serial PRIMARY KEY,';
echo '"Borough" varchar,';
echo '"CD_Number" int4,';
echo '"CD_Name" varchar,';
echo '"1970_Population" int8,';
echo '"1980_population" int8,';
echo '"1990_population" int8,';
echo '"2000_population" int8,';
echo '"2010_population" int8,'; 
echo
awk -F "," '{printf("INSERT INTO nyc_pops (Borough,CD_Number,CD_Name,1970_Population,1980_Population,1990_population,2000_population,2010_population, the_geom) VALUES ('\''%s'\'', %d, '\''%s'\'', %d, %d, %d, %d, %d);\n",$1,$2,$3,$4,$5,$6,$7,$8)}' nyc_pops.csv;

echo "COMMIT;"
~                  
