echo "BEGIN;";
echo 'CREATE TABLE "nypd_comp" (gid serial PRIMARY KEY,';
echo '"cmplnt_num" int4,';
echo '"cmplnt_fr_dt" date,';
echo '"cmplnt_fr_tm" time,';
echo '"cmplnt_to_dt" date,';
echo '"cmplnt_to_tm" time,';
echo '"rpt_dt" date,';
echo '"ky_cd" int4,';
echo '"ofns_desc" varchar,';
echo '"pd_cd" int4,';
echo '"pd_desc" varchar,';
echo '"crm_atpt_cptd_cd" varchar,';
echo '"law_cat_cd" varchar,';
echo '"juris_desc" varchar,';
echo '"boro_nm" varchar,';
echo '"addr_pct_cd" int4,';
echo '"loc_of_occur_desc" varchar,';
echo '"prem_typ_desc" varchar,';
echo '"parks_nm" varchar,';
echo '"hadevelopt" varchar,';
echo '"x_coord_cd" int4,';
echo '"y_coord_cd" int4,';
echo '"latitude" float8,';
echo '"longitude" float8,';
echo '"lat_lon" varchar);';
echo "SELECT AddGeometryColumn('','nypd_comp','the_geom','4326','POINT',2);";
echo
awk -F "," '{printf("INSERT INTO nypd_comp (cmplnt_num,cmplnt_fr_dt,cmplnt_fr_tm,cmplnt_to_dt,cmplnt_to_tm,rpt_dt,ky_cd,ofns_desc,pd_cd,pd_desc,crm_atpt_cptd_cd,law_cat_cd,juris_desc,boro_nm,addr_pct_cd,loc_of_occur_desc,prem_typ_desc,parks_nm,hadevelopt,x_coord_cd,y_coord_cd,latitude,longitude,lat_lon, the_geom) VALUES (%d, '\''%s'\'', '\''%s'\'', '\''%s'\'', '\''%s'\'', '\''%s'\'', %d, '\''%s'\'', %d,'\'' %s'\'', '\''%s'\'', '\''%s'\'','\''%s'\'', '\''%s'\'', %d, '\''%s'\'', '\''%s'\'', '\''%s'\'','\''%s'\'', %d, %d, %f, %f, '\''%s'\'', st_geomFromText('\''POINT (%d %d) '\'', 4326));\n",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$20,$21)}' nypd_comp.csv;

echo "COMMIT;" 
