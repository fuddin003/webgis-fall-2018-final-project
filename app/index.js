import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import {Image as ImageLayer} from 'ol/layer.js';
import {fromLonLat, toLonLat} from 'ol/proj.js';
import TileWMS from 'ol/source/TileWMS.js';
import ImageWMS from 'ol/source/ImageWMS.js';
import Projection from 'ol/proj/Projection.js';
import OSM from 'ol/source/OSM';




var projection = new Projection({
        code: 'EPSG:2263',
        extent: [913154.843600, 120114.582600, 1067382.510900, 272932.046000],
        units: 'ft'
      });

//addProjection(projection);

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM()
    }),
    new ImageLayer({
          extent: [-77.27,40.4,-71.75,41.31],
          source: new ImageWMS({
            url: '../mapscripts/template.php?',
            params: {
                      'LAYERS': 'nypp',
                      //'BBOX' : '913154.843600,120114.582600,1067382.510900,272932.046000',
                      'BBOX' : '-77.27,40.4,-71.75,41.31',
                      'SRS' : 'EPSG:4326'}
      })
    }),
    new ImageLayer({
          extent: [-77.27,40.4,-71.75,41.31],
          source: new ImageWMS({
            url: '../mapscripts/template.php?',
            params: {
                      'LAYERS': 'park_geo',
                      //'BBOX' : '913154.843600,120114.582600,1067382.510900,272932.046000',
                      'BBOX' : '-77.27,40.4,-71.75,41.31',
                      'SRS' : 'EPSG:4326'}
      })
    }),
    new ImageLayer({
          extent: [-77.27,40.4,-71.75,41.31],
          source: new ImageWMS({
            url: '../mapscripts/template.php?',
            params: {
                      'LAYERS': 'nypd_comp',
                      //'BBOX' : '913154.843600,120114.582600,1067382.510900,272932.046000',
                      'BBOX' : '-77.27,40.4,-71.75,41.31',
                      'SRS' : 'EPSG:4326'}
      })
    })
  ],
  view: new View({
    //projection: projection,
    projection: 'EPSG:4326',
    //center: fromLonLat([-73.9, 40.7]),
    center: [-73.9, 40.7],
    //center: [0,0],
    //center: [990268.843600, 196538.582600],
    zoom: 9
  })
});
