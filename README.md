# WEBGIS FALL 2018 Final Project


##Final Project Proposal



Maryam Akrami

Bharat Rosanlall

Anastasia Sagalovitch

Fahad Uddin

database access

/usr/local/pgsql/bin/psql -h 134.74.112.18 -d d308 -U uddi18
password: 

chekcout result

http://134.74.146.40/~[username]/webgis-fall-2018-final-project/index.html



Purpose of our project will be to use two datasets, NYC park properties and NYPD Complaints map to find out number of crimes and complaints in NYC park in a given time. Where we will use D3.js to visualize the crime details and google map api to pinpoint the location of the crime. We will be importing two datasets into the postegre sql to do relational mapping.

nyc park properties
https://data.cityofnewyork.us/City-Government/Parks-Properties/k2ya-ucmv

nyc parks crime statistics
https://www1.nyc.gov/site/nypd/stats/crime-statistics/park-crime-stats.page

nyc compalints
https://data.cityofnewyork.us/Public-Safety/NYPD-Complaint-Map-Year-to-Date-/2fra-mtpn

nyc populations data by CD
https://data.cityofnewyork.us/City-Government/New-York-City-Population-By-Community-Districts/xi7c-iiu2


Openlayers version 5.3.0


D3 version 4
https://github.com/d3/d3

D3 geographies
https://github.com/d3/d3/blob/master/API.md#geographies-d3-geo

D3 examples

sample openlayers
https://openlayers.org/en/latest/examples/d3.html

http://bl.ocks.org/mbertrand/5218300

http://bl.ocks.org/pbogden/6283017

https://bost.ocks.org/mike/leaflet/

http://blog.mastermaps.com/2015/11/mapping-grid-based-statistics-with.html

d3 tutorials

https://www.lynda.com/D3-js-tutorials/D3-js-Essential-Training-Data-Scientists/504428-2.html

https://www.lynda.com/D3-js-tutorials/Learning-Data-Visualization-D3-js/594451-2.html


NodeJS version v10.14.1

to start project:
npm init
npm install ol
npm install --save-dev parcel-bundler

parcel-builder will help us to package the solution into dist folder

checkout my example at http://134.74.146.40/~rosa18/webgis-fall-2018-final-project/app/dist/index.html


Ana 12/6/18 7:17 PM 
Adding the newest shapefile for NYC Parks Properties  which includes playgrounds per group meeting discussion
source url: https://data.cityofnewyork.us/City-Government/Parks-Properties/k2ya-ucmv

Uploaded the ~/ parks_playgrounds/geo_export_cfa5fe57-9f50-4536-b84d-996cfdafd8dc.shp into Fahad's database on the class server which the group is using for the class project. 
The table is called "project".
8:12 PM - New table "parks_playgrounds_4326" uploaded to Fahad's database for the group 
----
queries


1. show all the parks as polygon in nyc

2. show all the nyc parks as polygon and all the crimes inside the park as points (when user click on a point(crime that occured) user will see the date, time, type of crime)

3. show all the nyc parks as polygon and all the crimes inside the park based on date and time (user can change the date and time, then click on the point to see the type of the crime)

4. show the population density within the park areas(polygon)/ precinct (here when click on the park polygon, user will see the population density)

5. show the population and crime density within the park areas within the precinct(here when click on the park polygon user will see the population and crime density like the second project)

----- 
